from .config import KARL_MODEL, USERNAME, PASSWORD, CLIENT_SECRET
import ml
import serial
import asyncio


class Karl:
    def __init__(self, is_gps, is_log=False):
        self.thing = None
        self.__is_gps = is_gps
        self.__is_log = is_log 
        self.__longitude = 0
        self.__latitude = 0 
        if self.__is_gps:
            self._ser = serial.Serial("/dev/ttyAMA0", 9600)
        if self.__is_log:
            ml.setup_logger("Karl")

    def run(self):
        self.thing = ml.create_thing(
            model_json=KARL_MODEL,
            grant_type="password",
            username=USERNAME,
            password=PASSWORD,
            oauth2_secret=CLIENT_SECRET,
            is_broker=True,
            is_repository=True
        )
        self.thing.add_on_thing_start_ok_callback(self.update_position, True, False)
        self.thing.run_forever()

    def update_position(self):
        if self.__is_gps:
            self.read_position()
            print(self.__longitude)
            self.thing.entry.ditto_features["id1"].value = self.__longitude
            self.thing.entry.ditto_features["id2"].value = self.__latitude
        self.thing.loop.call_later(0.1, self.update_position)

    @staticmethod
    def nmea2latlong(nmea):
        pos1 = nmea.find(str.encode("$GPRMC"))
        pos2 = nmea.find(str.encode("\n"))
        loc = nmea[pos1:pos2]
        data = loc.split(str.encode(","))
        if len(data) > 5:
            lat2_dms = float(data[3])
            long2_dms = float(data[5])

            # convert to the real latitude and longitude
            latitude = lat2_dms * 0.337042402315857 + 51.45262998
            longitude = long2_dms * 0.526621755422343 + 7.99501874
        else:
            latitude = 0
            longitude = 0
        return [latitude, longitude]

    def read_position(self):
        if self.__is_gps:
            try:
                gps = self._ser.readline()
                [self.__latitude, self.__longitude] = self.nmea2latlong(gps)

            except serial.SerialException:
                return None
