KARL_MODEL = {
    "thingId": "s3i:5a256363-4e77-4436-8c14-20b147b38819",
    "policyId": "s3i:5a256363-4e77-4436-8c14-20b147b38819",
    "attributes": {
        "class": "ml40::Thing",
        "name": "gSFL Waldarbeiter Karl",
        "roles": [{"class": "fml40::ForestWorker"}],
        "features": [
            {
                "class": "ml40::PersonalName",
                "firstName": "Karl",
                "lastName": "Klammer"
            },
            {
                "class": "ml40::Location",
                "longitude": "ditto-feature:id1",
                "latitude": "ditto-feature:id2"
            },
            {
                "class": "fml40::AcceptsSingleTreeFellingJobs"
            },
            {
                "class": "AcceptsMoveCommands"
            },
            {
                "class": "ml40::Address",
                "country": "Germany",
                "streetnumber": 123,
                "city": "Arnsberg",
                "zip": 59755,
                "street": "Hauptstr."
            }
        ]
    },
    "features": {
        "id1": {
            "properties": {
                "longitude": 7.4063186
             }
        },
        "id2": {
            "properties": {
                "latitude": 50.775555
            }
        }
    }
}

CLIENT_SECRET = "558cf99a-377e-4551-a099-1085decd5948"
USERNAME = "Karl"
PASSWORD = "ccODpRlgNkaOy1TQR3t7"
