from .config import HMI_MODEL, CLIENT_SECRET
import ml
from s3i.messages import ServiceRequest
import uuid
import json
import time

class HMI:
    def __init__(self, direction, speed):
        self.thing = None
        self.__direction = direction
        self.__speed = speed

    def run(self):
        self.thing = ml.create_thing(
            model=HMI_MODEL,
            grant_type="client_credentials",
            secret=CLIENT_SECRET,
            is_broker_rest=False,
            is_broker=True,
            is_repo=False
        )
        self.thing.add_user_def(func=self.send_req)
        self.thing.run_forever()

    def send_req(self):
        ser_req = ServiceRequest()
        ser_req.fillServiceRequest(
            senderUUID=HMI_MODEL.get("thingId", None),
            receiverUUIDs = ["s3i:9a6c2c61-0a01-4571-b6cf-ef4a7b37ac9c"],
            sender_endpoint= "s3ib://" + HMI_MODEL.get("thingId", None),
            serviceType="fml40::AcceptsMoveCommands/move",
            parameters= {"direction": self.__direction,
                         "speed": self.__speed},
            msgUUID="s3i:{}".format(uuid.uuid4())

        )
        while True:
            if self.thing.broker is None:
                continue
            else:
                self.thing.broker.send(["s3ib://s3i:9a6c2c61-0a01-4571-b6cf-ef4a7b37ac9c"], json.dumps(ser_req.msg))
            time.sleep(1)
