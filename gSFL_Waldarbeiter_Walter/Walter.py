from .config import WALTER_MODEL, USERNAME, PASSWORD, CLIENT_ID, CLIENT_SECRET
import ml
import serial
import asyncio
from .MPU9250 import MPU9250


class Walter:
    def __init__(self, is_gps, is_imu, is_log=False):
        self.thing = None
        self.__is_gps = is_gps
        self.__is_log = is_log
        self.__is_imu = is_imu
        self._longitude = 0
        self._latitude = 0
        if self.__is_imu:
            self.__imu = MPU9250()
        if self.__is_gps:
            self._ser = serial.Serial("/dev/ttyAMA0", 9600)
        if self.__is_log:
            ml.setup_logger("Walter")


    def run(self):
        self.thing = ml.create_thing(
            model_json=WALTER_MODEL,
            grant_type="password",
            username=USERNAME,
            password=PASSWORD,
            oauth2_secret=CLIENT_SECRET,
            is_broker=True,
            is_repository=True
        )
        self.thing.add_on_thing_start_ok_callback(self.update_position, True, False)
        self.thing.add_on_thing_start_ok_callback(self.update_health_status, True, False)
        self.thing.run_forever()

    def update_health_status(self):
        if self.__is_imu:
            accel = self.read_accel()
            if accel.get('x') is not None:
                 if -1 < accel['x'] < 0.4:
                     self.thing.entry.ditto_features['id3'].value = False
                 else:
                     self.thing.entry.ditto_features['id3'].value = True 
            self.thing.loop.call_later(0.5, self.update_health_status)
    
    def read_accel(self):
       accel = self.__imu.readAccel()
       return accel

    def update_position(self):
        if self.__is_gps:
            self.read_position()
            self.thing.entry.ditto_features["id1"].value = self._longitude
            self.thing.entry.ditto_features["id2"].value = self._latitude
        self.thing.loop.call_later(0.5, self.update_position)

    @staticmethod
    def nmea2latlong(nmea):
        pos1 = nmea.find(str.encode("$GPRMC"))
        pos2 = nmea.find(str.encode("\n"))
        loc = nmea[pos1:pos2]
        data = loc.split(str.encode(","))
        if len(data) > 5:
            lat2_dms = float(data[3])
            long2_dms = float(data[5])

            # convert to the real latitude and longitude
            latitude = lat2_dms * 0.337042402315857 + 51.45262998
            longitude = long2_dms * 0.526621755422343 + 7.99501874
        else:
            latitude = 0
            longitude = 0
        return [latitude, longitude]

    def read_position(self):
        if self.__is_gps:
            try:
                if self._ser.inWaiting() > 100:
                    gps = self._ser.readline()
                    [self._latitude, self._longitude] = self.nmea2latlong(gps)
                else:
                    return None
            except serial.SerialException:
                return None

