WALTER_MODEL = {
    "thingId": "s3i:0c253262-428e-44be-a11a-b83566bd1f68",
    "policyId": "s3i:0c253262-428e-44be-a11a-b83566bd1f68",
    "attributes": {
        "class": "ml40::Thing",
        "roles": [{"class": "fml40::ForestWorker"}],
        "name": "gSFL Waldarbeiter Walter",
        "features": [
            {
                "class": "ml40::Location",
                "longitude": "ditto-feature:id1",
                "latitude": "ditto-feature:id2"
            },
            {
                "class": "ml40::PersonalName",
                "firstName": "Walter",
                "lastName": "Winkler"
            },
            {
                "class": "fml40::AcceptsSingleTreeFellingJobs"
            },
            {
                "class": "ml40::Composite",
                "targets": [
                    {
                        "class": "ml40::Thing",
                        "name": "Vitalsensor", 
                        "roles": [{"class": "fml40::VitalitySensor"}],
                        "features": [
                            {
                                "class": "fml40::VitalityStatus",
                                "ok": "ditto-feature:id3"
                            }
                        ]
                    }
                ]
            }
        ]
    },
    "features": {
        "id1": {
            "properties": {
                "longitude": 7.995590651226388
            }
        },
        "id2": {
            "properties": {
                "latitude": 51.45267244734269
            }
        },
        "id3": {
            "properties": {
                "ok": True
            }
        }
    }
}

USERNAME = "Walter"
PASSWORD = "6qxIfVRYy7JpLgtjbtn3"
CLIENT_ID = "s3i:0c253262-428e-44be-a11a-b83566bd1f68"
CLIENT_SECRET = "8d9ad853-2bfb-44f1-ad47-bc2a6bad8a03"
