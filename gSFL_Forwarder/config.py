MMI_FORWARDER_MODEL = {
    "thingId": "s3i:08bf8c98-ad4e-4bc1-b823-7667e73688ac",
    "policyId": "s3i:08bf8c98-ad4e-4bc1-b823-7667e73688ac",
    "attributes": {
        "class": "ml40::Thing",
        "name": "gSFL MMI Forwarder",
        "roles": [{"class": "fml40::Forwarder"}],
        "features": [
            {
                "class": "ml40::Composite",
                "targets": [
                    {
                        "class": "ml40::Thing",
                        "name": "Waage am Forwarder",
                        "roles": [{"class": "ml40::Scale"}],
                        "features": [
                            {
                                "class": "ml40::Weight",
                                "weight": "ditto-feature:id1"
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "name": "Tank des Forwarders",
                        "roles": [{"class": "fml40::Tank"}],
                        "features": [
                            {
                                "class": "ml40::LiquidFillingLevel",
                                "maxLevel": "ditto-feature:id2",
                                "currentLevel": "ditto-feature:id3"
                            }
                        ]
                    }
                ]
            },
            {
                "class": "ml40::Dimensions",
                "weight": 15000
            },
            {
                "class": "fml40::Forwards"
            },
            {
                "class": "ml40::Location",
                "longitude": "ditto-feature:id4",
                "latitude": "ditto-feature:id5"
            }
        ]
    },
    "features": {
        "id1": {
            "properties": {
                "weight": 300
            }
        },
        "id2": {
            "properties": {
                "maxLevel": 300,
            }
        },
        "id3": {
            "properties": {
                "currentLevel": 100
            }
        },
        "id4": {
            "properties": {
                "longitude": 7.995154608412899
            }
        },
        "id5": {
            "properties": {
                "latitude": 51.452985896776845
            }
        }
    }
}

CLIENT_SECRET = "359d9940-6c75-4330-8141-6307ec283d31"
