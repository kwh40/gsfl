from .config import HARVESTER_MODEL, CLIENT_SECRET
import ml
import serial
import asyncio


class Harvester:
    def __init__(self, is_gps, is_log=False):
        self.thing = None
        self.__is_gps = is_gps
        self.__is_log = is_log
        self._longitude = 0
        self._latitude = 0
        if self.__is_gps:
            self._ser = serial.Serial("/dev/ttyAMA0", 9600)
        if self.__is_log:
            ml.setup_logger("gSFL_Harvester")

    def run(self):
        self.thing = ml.create_thing(
            model_json=HARVESTER_MODEL,
            oauth2_secret=CLIENT_SECRET,
            is_repository=True,
            is_broker=True)

        self.thing.add_on_thing_start_ok_callback(self.simulate_rpm, True, False)
        self.thing.add_on_thing_start_ok_callback(self.update_position, True, False)
        self.thing.run_forever()

    """
    async def await_coro_later(self, delay, coro, *args, **kwargs):
        await asyncio.sleep(delay)
        await coro(*args, **kwargs)
    """

    def simulate_rpm(self, tank="up"):
        my_engine = self.thing.entry.ditto_features["id1"]
        if tank == "down":
          my_engine.value -= 100
          if my_engine.value < 2000:
             tank = "up"

        elif tank == "up":
          my_engine.value += 100
          if my_engine.value > 2500:
             tank = "down"
        self.thing.loop.call_later(10, self.simulate_rpm, tank)

    def update_position(self):
        longitude = self.thing.entry.ditto_features["id2"]
        latitude = self.thing.entry.ditto_features["id3"]
        if self.__is_gps:
            self.read_position()
            longitude.value = self._longitude
            latitude.value = self._latitude
            self.thing.loop.call_later(0.5, self.update_position)

    @staticmethod
    def nmea2latlong(nmea):
        pos1 = nmea.find(str.encode("$GPRMC"))
        pos2 = nmea.find(str.encode("\n"))
        loc = nmea[pos1:pos2]
        data = loc.split(str.encode(","))
        if len(data) > 5:
            lat2_dms = float(data[3])
            long2_dms = float(data[5])

            # convert to the real latitude and longitude
            latitude = lat2_dms * 0.337042402315857 + 51.45262998
            longitude = long2_dms * 0.526621755422343 + 7.99501874
        else:
            latitude = 0
            longitude = 0
        return [latitude, longitude]

    def read_position(self):
        if self.__is_gps:
            try:
                if self._ser.inWaiting() > 100:
                    gps = self._ser.readline()
                    [self._latitude, self._longitude] = self.nmea2latlong(gps)
                else:
                    return None
            except serial.SerialException:
                return None
