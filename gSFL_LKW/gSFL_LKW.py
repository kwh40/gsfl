from .config import LKW_MODEL, CLIENT_SECRET
import ml
import serial
from ml.fml40.features.functionalities.accepts_move_commands import AcceptsMoveCommands
from bricknil import attach, start
from bricknil.hub import CPlusHub
from bricknil.sensor.motor import CPlusLargeMotor, CPlusXLMotor
from bricknil.sensor.light import LED
from bricknil.sensor import Button
from bricknil.const import Color 
from curio import sleep
from enum import Enum
import asyncio
import threading 


class Limit(Enum):
    Move = 100
    Turn = 25


class AcceptsMoveCommandsImpl(AcceptsMoveCommands):
    def __init__(self,  name="a", identifier="b", bt_controller=None):
        super(AcceptsMoveCommands, self).__init__(
            name=name,
            identifier=identifier
        )
        self.__bt_controller = bt_controller

    async def move(self, direction, speed):
        print("i am moving in direction {} with speed {}".format(direction, speed))
        self.__bt_controller.moving_control(direction, speed)


@attach(LED, name="led")
@attach(Button, name="bt", capabilities=["sense_press"])
@attach(CPlusXLMotor, name="motor_bewegung", capabilities=['sense_speed'], port=0)
@attach(CPlusLargeMotor, name="motor_abbiegung", capabilities=['sense_speed'], port=1)
class LKW(CPlusHub):
    def __init__(self, is_gps, is_log, name="lkw", query_port_info=False, ble_id=None):
        super().__init__(name, query_port_info, ble_id)
        self.thing = None
        self.__is_gps = is_gps
        self.__is_log = is_log
        self.__port_0_speed = 0
        self.__port_1_speed = 0
        self.__port_0_ist_speed = 0
        self.__port_1_ist_speed = 0

        self.__latitude = 0
        self.__longitude = 0
        if self.__is_gps:
            self._ser = serial.Serial("/dev/ttyAMA0", 9600)
        if self.__is_log:
            ml.setup_logger("gSFL_LKW")

        self.close_bt_connection = False
        self.__step_time = 0.5

    async def bt_change(self):
        is_pressed = self.bt.value[Button.capability.sense_press]
        if is_pressed:
             self.close_bt_connection = True
             if self.thing is not None:
                  self.thing.loop.stop()

    async def motor_bewegung_change(self):
        speed = self.motor_bewegung.value[CPlusXLMotor.capability.sense_speed]
        if 0 <= speed <= 127:
            self.__port_0_ist_speed = speed
        elif 127 <= speed <= 255:
            self.__port_0_ist_speed = speed - 255
        print("is port 0:{}".format(self.__port_0_ist_speed))

    async def motor_abbiegung_change(self):
        # Positive value for right and negative value for left
        speed = self.motor_abbiegung.value[CPlusLargeMotor.capability.sense_speed]
        if 0 <= speed <= 127:
            self.__port_1_ist_speed = speed
        elif 127 <= speed <= 255:
            self.__port_1_ist_speed = speed - 255
        print("is post 1:{}".format(self.__port_1_ist_speed))

    async def initial(self):
        await self.led.set_color(Color.green)
        await sleep(10)
        await self.led.set_color(Color.blue)

    async def dispatch(self):
       
        while True:
            print("port 0:{}".format(self.__port_0_speed))
            print("port 1:{}".format(self.__port_1_speed))
            print("bt: {}".format(self.close_bt_connection))
            await self.motor_bewegung.set_speed(int(self.__port_0_speed))
            await self.motor_abbiegung.set_speed(int(self.__port_1_speed))
            await sleep(self.__step_time)
            if self.close_bt_connection:
               print("quit")
               break 
       
       # await self.motor_bewegung.set_speed(int(self.__port_0_speed))
       # await self.motor_abbiegung.set_speed(int(self.__port_1_speed))
       # await self.await_coro_later(self.__step_time, self.dispatch)        


    async def run(self):
        await self.initial()
        threading.Thread(target=self.__run_thing).start()
        await self.dispatch()


    def moving_control(self, direction, speed):
        if 0 <= direction <= 90:
            self.__port_0_speed = Limit.Move.value * speed
            self.__port_1_speed = Limit.Turn.value / 90 * direction

        elif 90 < direction <= 180:
            self.__port_0_speed = - Limit.Move.value * speed
            self.__port_1_speed = Limit.Turn.value / 90 * (180 - direction)

        elif 180 < direction <= 270:
            self.__port_0_speed = - Limit.Move.value * speed
            self.__port_1_speed = - Limit.Turn.value / 90 * (direction - 180)

        elif 270 < direction < 360:
            self.__port_0_speed = Limit.Move.value * speed
            self.__port_1_speed = - Limit.Turn.value / 90 * (360 - direction)

    def run_thing(self):
        self.thing = ml.create_thing(
            model_json=LKW_MODEL,
            oauth2_secret=CLIENT_SECRET,
            is_broker=True,
            is_repository=True
        )

        self.thing.add_ml40_implementation(
            AcceptsMoveCommandsImpl,
            "fml40::AcceptsMoveCommands",
            bt_controller=self
        )
        self.thing.add_on_thing_start_ok_callback(
            self.update_position, True, False
        )
        self.thing.add_on_thing_start_ok_callback(
            self.update_motor_rpm, True, False
        )
        self.thing.run_forever()

    def update_position(self):
        if self.__is_gps:
            self.read_position()
            self.thing.entry.ditto_features["id1"].value = self.__longitude
            self.thing.entry.ditto_features["id2"].value = self.__latitude
            self.thing.loop.call_later(0.1, self.update_position)

    def update_motor_rpm(self):
        self.thing.entry.ditto_features["id3"].value = self.__port_0_ist_speed
        self.thing.entry.ditto_features["id4"].value = self.__port_1_ist_speed
        self.thing.loop.call_later(0.1, self.update_motor_rpm)

    @staticmethod
    def nmea2latlong(nmea):
        pos1 = nmea.find(str.encode("$GPRMC"))
        pos2 = nmea.find(str.encode("\n"))
        loc = nmea[pos1:pos2]
        data = loc.split(str.encode(","))
        if len(data) > 5:
            lat2_dms = float(data[3])
            long2_dms = float(data[5])

            # convert to the real latitude and longitude
            latitude = lat2_dms * 0.337042402315857 + 51.45262998
            longitude = long2_dms * 0.526621755422343 + 7.99501874
        else:
            latitude = 0
            longitude = 0
        return [latitude, longitude]

    def read_position(self):
        if self.__is_gps:
            try:
                if self._ser.inWaiting() > 100:
                    gps = self._ser.readline()
                    [self.__latitude, self.__longitude] = self.nmea2latlong(gps)
                else:
                    return None
            except serial.SerialException:
                return None

