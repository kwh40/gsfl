LKW_MODEL = {
    "thingId": "s3i:9a6c2c61-0a01-4571-b6cf-ef4a7b37ac9c",
    "policyId": "s3i:9a6c2c61-0a01-4571-b6cf-ef4a7b37ac9c",
    "attributes": {
        "class": "ml40::Thing",
        "name": "gSFL LKW",
        "roles": [{"class": "fml40::LogTruck"}],
        "features": [
            {
                "class": "fml40::TransportsLogs"
            },
            {
                "class": "ml40::AcceptsJobs"
            },
            {
                "class": "ml40::Composite",
                "targets": [
                    {
                        "class": "ml40::Thing",
                        "name": "Ladeflaeche",
                        "roles": [{"class": "fml40::LogLoadingArea"}],
                        "features": [
                            {
                                "class": "ml40::Weight",
                                "weight": 1000
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "name": "Motor zur Bewegung",
                        "roles": [{"class": "fml40::Engine"}],
                        "features": [
                            {
                                "class": "fml40::RotationalSpeed",
                                "rpm": "ditto-feature:id3"
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "name": "Motor zum Ablenken",
                        "roles": [{"class": "fml40::Engine"}],
                        "features": [
                            {
                                "class": "fml40::RotationalSpeed",
                                "rpm": "ditto-feature:id4"
                            }
                        ]
                    }
                ]
            },
            {
                "class": "ml40::Location",
                "longitude": "ditto-feature:id1",
                "latitude": "ditto-feature:id2"
            },
            {
                "class": "ml40::Weight",
                "weight": 0.0
            },
            {
                "class": "ml40::Dimensions",
                "length": 0.0,
                "width": 0.0,
                "height": 0.0
            },
            {
                "class": "fml40::AcceptsMoveCommands"
            }
        ]
    },
    "features": {
        "id1": {
            "properties": {
                "longitude": 6.08361
            }
        },
        "id2": {
            "properties": {
                "latitude": 50.775555
            }
        },
        "id3": {
            "properties": {
                "rpm": 0
            }
        },
        "id4": {
            "properties": {
                "rpm": 0
            }
        }
    }
}
CLIENT_SECRET = "53427cd4-7248-492c-9f6f-a14e6d996059"

