import threading
from .config import MINI_TRACTOR_DATE_MODEL, CLIENT_SECRET
import serial
import asyncio
from ml.fml40.features.functionalities.accepts_move_commands import AcceptsMoveCommands
from ml.fml40.features.functionalities.accepts_shield_commands import AcceptsShieldCommands
from ml.fml40.features.functionalities.accepts_winch_command import AcceptsWinchCommands
from ml import APP_LOGGER, setup_logger, create_thing
from curio import sleep
from bricknil import attach, start
from bricknil.hub import CPlusHub
from bricknil.sensor.motor import CPlusLargeMotor, CPlusXLMotor
from bricknil.sensor.light import LED
from bricknil.sensor import Button 
from bricknil.const import Color
from enum import Enum



class AcceptsMoveCommandsImpl(AcceptsMoveCommands):
    def __init__(self, name="", identifier="", controller=None):
        super().__init__(
            name=name,
            identifier=identifier
        )
        self.controller = controller

    async def move(self, direction, speed):
        APP_LOGGER.info("[gSFL][MiniTractor]: I am moving now, direction: {}  speed: {}".format(direction, speed))
        self.controller.moving_control(direction=direction, speed_level=speed)


class AcceptsWinchCommandsImpl(AcceptsWinchCommands):
    def __init__(self, name="", identifier="", controller=None):
        self.controller = controller
        super(AcceptsWinchCommands, self).__init__(
            name=name,
            identifier=identifier)

    async def rollUp(self, speed):
        APP_LOGGER.info("[gSFL][MiniTractor]: I am rolling the cable winch up with speed {}".format(speed))
        self.controller.winch_control(operation="rollUp", speed_level=speed)

    async def rollDown(self, speed):
        APP_LOGGER.info("[gSFL][MiniTractor]: I am rolling the cable winch down with speed {}".format(speed))
        self.controller.winch_control(operation="rollDown", speed_level=speed)


class AcceptsShieldCommandsImpl(AcceptsShieldCommands):
    def __init__(self, name="", identifier="", controller=None):
        self.controller = controller
        super(AcceptsShieldCommands, self).__init__(
            name=name,
            identifier=identifier
        )

    async def shieldUp(self, speed):
        APP_LOGGER.info("[gSFL][MiniTractor]: I am raising the shield up with speed {}".format(speed))
        self.controller.shield_control(operation="shieldUp", speed_level=speed)

    async def shieldDown(self, speed):
        APP_LOGGER.info("[gSFL][MiniTractor]: I am sinking the shield down with speed {}".format(speed))
        self.controller.shield_control(operation="shieldDown", speed_level=speed)


class SpeedLevel(Enum):
    MOTOR_SPEED_LIMIT = 80
    SHIELD_SPEED_LIMIT = 40
    WINCH_SPEED_LIMIT = 30


@attach(LED, name="led")
@attach(Button, name="bt", capabilities=["sense_press"])
@attach(CPlusXLMotor, name="right_motor", capabilities=['sense_speed'], port=0)
@attach(CPlusXLMotor, name="left_motor", capabilities=['sense_speed'], port=1)
@attach(CPlusLargeMotor, name="winch", capabilities=['sense_speed', ('sense_pos', 10)], port=2)
@attach(CPlusLargeMotor, name="shield", capabilities=['sense_speed', ('sense_pos', 5)], port=3)
class Raupe(CPlusHub):
    def __init__(self, is_gps, is_log, led_ind, name="raupe", query_port_info=False, ble_id=None):
        super().__init__(name, query_port_info, ble_id)

        self.__is_gps = is_gps
        self.__is_log = is_log
        self.__led_ind = led_ind # LED indicator to show whether the raupe is launched 
        self.__longitude = 0
        self.__latitude = 0
        if self.__is_gps:
            self._ser = serial.Serial("/dev/ttyS0", 9600)
        if self.__is_log:
            setup_logger("gSFL_Rueckeraupe")

        # initial port speed
        self.__port_0_speed = 0
        self.__port_1_speed = 0
        self.__port_2_speed = 0
        self.__port_3_speed = 0

        # step time
        self.__step_time = 0.5  # 0.5s

        # ist value

        self.__right_motor_ist_speed = 0
        self.__left_motor_ist_speed = 0
        self.__winch_ist_speed = 0
        self.__winch_length = 0

        self.__shield_ist_speed = 0
        self.__shield_lift = 0

        # is winch and shield in initial state
        self.__is_winch_init = False
        self.__is_shield_init = False

        # position in initial state
        self.__winch_initial_state_pos = 0
        self.__shield_initial_state_pos = 0

        # close bluetooth connection
        self.close_bluetooth_connection = False

        # Mini Tractor orientation
        self.__orientation = 0
        
    async def bt_change(self):
        """This function will be executed when the bt on the hub is pressed
        """
        is_pressed = self.bt.value[Button.capability.sense_press]
        if is_pressed:
           self.close_bluetooth_connection = True 
           if self.thing is not None:
               self.thing.loop.stop()

    async def right_motor_change(self):
        """This function will be executed when the current state of right motor is changed.
        """
        speed = self.right_motor.value[CPlusXLMotor.capability.sense_speed]
        if 0 <= speed <= 127:
            self.__right_motor_ist_speed = speed
        elif 127 < speed <= 255:
            self.__right_motor_ist_speed = speed - 255

        # print("right motor speed: {}".format(self.__right_motor_ist_speed))

    async def left_motor_change(self):
        """This function will be executed when the current state of left motor is changed.
        """
        speed = self.left_motor.value[CPlusXLMotor.capability.sense_speed]
        if 0 <= speed <= 127:
            self.__left_motor_ist_speed = speed
        elif 127 <= speed <= 255:
            self.__left_motor_ist_speed = speed - 255

        # print("left motor speed {}".format(self.__left_motor_ist_speed))

    async def winch_change(self):
        """This function will be executed when the current state of winch is changed.
        """
        speed = self.winch.value[CPlusLargeMotor.capability.sense_speed]
        pos = self.winch.value[CPlusLargeMotor.capability.sense_pos]

        if 0 <= speed < 127:
            self.__winch_ist_speed = speed
        elif 127 <= speed <= 255:
            self.__winch_ist_speed = speed - 255

        if self.__is_winch_init:
            if 4294967298 > pos >= (self.__winch_initial_state_pos - 500000):
                self.__winch_length = (pos - self.__winch_initial_state_pos) / 1800 * 16
                if self.__winch_length >= 1000:
                    self.__winch_length = 0
            else:
                self.__winch_length = (4294967298 - self.__winch_initial_state_pos + pos) / 1800 * 16

        if self.__winch_length < 0:
            self.__winch_length = 0
        elif self.__winch_length > 50:
            self.__winch_length = 50

    async def shield_change(self):
        """This function will be executed when the current state of shield is changed
        """
        speed = self.shield.value[CPlusLargeMotor.capability.sense_speed]
        pos = self.shield.value[CPlusLargeMotor.capability.sense_pos]

        if 0 <= speed < 127:
            self.__shield_ist_speed = speed
        elif 127 <= speed <= 255:
            self.__shield_ist_speed = speed - 255

        if self.__is_shield_init:
            if 4294967298 > pos > (self.__shield_initial_state_pos - 500000):
                self.__shield_lift = 3.7 - (pos - self.__shield_initial_state_pos) / 3800 * 3.7
                if self.__shield_lift <= -1000:
                    self.__shield_lift = 3.7
            else:
                self.__shield_lift = 3.7 - (4294967298 - self.__shield_initial_state_pos + pos) / 3800 * 3.7

        """
        Messfehler 
        """

        if self.__shield_lift < -0:
            self.__shield_lift = 0
        elif self.__shield_lift > 3.7:
            self.__shield_lift = 3.7

    async def initial(self):
        """Function to extra initialization for the mini tractor
        """
        await self.winch.set_speed(-15)
        await self.shield.set_speed(-15)
        await sleep(30)  # To start up the mini tractor and lego technic hub, it needs approx. 30 sec.
        await self.led.set_color(Color.green)
        while True:
            if self.__winch_ist_speed == 0:
                self.__is_winch_init = True
                self.__winch_length = 0

            if self.__shield_ist_speed == 0:
                self.__is_shield_init = True
                self.__shield_lift = 3.7

            if self.__is_winch_init and self.__is_shield_init:
                self.__winch_initial_state_pos = self.winch.value[CPlusLargeMotor.capability.sense_pos]
                self.__shield_initial_state_pos = self.shield.value[CPlusLargeMotor.capability.sense_pos]
                await self.led.set_color(Color.blue)
                await self.winch.set_speed(0)
                await self.shield.set_speed(0)
                break
            await sleep(1)

    async def dispatch(self):
        while True:
            """
            Sicherheitsmaßnahmen 
            """
            if self.__winch_length == 0 and self.__port_2_speed < 0:
                self.__port_2_speed = 0

            elif self.__winch_length == 50 and self.__port_2_speed > 0:
                self.__port_2_speed = 0

            if self.__shield_lift == 0 and self.__port_3_speed > 0:
                self.__port_3_speed = 0

            elif self.__shield_lift == 3.7 and self.__port_3_speed < 0:
                self.__port_3_speed = 0

            if self.__port_0_speed != 0:
                print("port 0: {}".format(self.__port_0_speed))
                self.__port_2_speed = 0
                self.__port_3_speed = 0
            if self.__port_1_speed != 0:
                print("port 1: {}".format(self.__port_1_speed))
                self.__port_2_speed = 0
                self.__port_3_speed = 0
            if self.__port_2_speed != 0:
                print("port 2: {}".format(self.__port_2_speed))
                self.__port_0_speed = 0
                self.__port_1_speed = 0
            if self.__port_3_speed != 0:
                print("port 3: {}".format(self.__port_3_speed))
                self.__port_0_speed = 0
                self.__port_1_speed = 0

            await self.left_motor.set_speed(int(self.__port_1_speed))
            await self.right_motor.set_speed(int(self.__port_0_speed))
            await self.winch.set_speed(int(self.__port_2_speed))
            await self.shield.set_speed(int(self.__port_3_speed))
            await sleep(self.__step_time)
            if self.close_bluetooth_connection:
                break

    async def run(self):
        """This function will be executed if the technic hub is started
        """
        await self.initial()
        threading.Thread(target=self.__run_thing).start()
        await self.dispatch()

    def __run_thing(self):
        self.thing = create_thing(
            model_json=MINI_TRACTOR_DATE_MODEL,
            oauth2_secret=CLIENT_SECRET,
            is_broker=True,
            is_repository=True
        )
        self.thing.add_on_thing_start_ok_callback(
            self.update_position,
            True,
            False
        )
        self.thing.add_on_thing_start_ok_callback(
            self.update_winch_shield,
            True,
            False
        )
        self.thing.add_on_thing_start_ok_callback(
            self.update_speed,
            True,
            False
        )

        self.thing.add_ml40_implementation(
            AcceptsMoveCommandsImpl, "fml40::AcceptsMoveCommands", controller=self
        )
        self.thing.add_ml40_implementation(
            AcceptsWinchCommandsImpl, "fml40::AcceptsWinchCommands", controller=self
        )
        self.thing.add_ml40_implementation(
            AcceptsShieldCommandsImpl, "fml40::AcceptsShieldCommands", controller=self
        )
        self.__led_ind.on()
        self.thing.run_forever()

    def update_speed(self):
        self.thing.entry.ditto_features["id5"].value = self.__left_motor_ist_speed
        self.thing.entry.ditto_features["id6"].value = self.__right_motor_ist_speed
        self.thing.loop.call_later(0.5, self.update_speed)

    def update_winch_shield(self):
        self.thing.entry.ditto_features["id4"].value = self.__winch_length
        self.thing.entry.ditto_features["id8"].value = self.__shield_lift
        self.thing.loop.call_later(0.5, self.update_winch_shield)

    def update_position(self):
        if self.__is_gps:
            self.read_position()
            self.thing.entry.ditto_features["id1"].value = self.__longitude
            self.thing.entry.ditto_features["id2"].value = self.__latitude
        self.thing.loop.call_later(0.1, self.update_position)

    @staticmethod
    def nmea2latlong(nmea):
        pos1 = nmea.find(str.encode("$GPRMC"))
        pos2 = nmea.find(str.encode("\n"))
        loc = nmea[pos1:pos2]
        data = loc.split(str.encode(","))
        if len(data) > 5:
            lat2_dms = float(data[3])
            long2_dms = float(data[5])

            # convert to the real latitude and longitude
            latitude = lat2_dms * 0.337042402315857 + 51.45262998
            longitude = long2_dms * 0.526621755422343 + 7.99501874
        else:
            latitude = 0
            longitude = 0
        return [latitude, longitude]

    def read_position(self):
        if self.__is_gps:
            try:
                if self._ser.inWaiting() > 100:
                     gps = self._ser.readline()
                     [self.__latitude, self.__longitude] = self.nmea2latlong(gps)
                     print(self.nmea2latlong(gps))
                else:
                     return None
            except serial.SerialException:
                return None

    def moving_control(self, direction, speed_level):
        direction = float(direction)
        speed_level = float(speed_level)
        self.__orientation = direction
        if 90 >= direction >= 0:
            self.__port_1_speed = - (SpeedLevel.MOTOR_SPEED_LIMIT.value + direction / 2) * speed_level
            self.__port_0_speed = speed_level * (SpeedLevel.MOTOR_SPEED_LIMIT.value - direction / 2)

        elif 180 >= direction > 90:
            self.__port_1_speed = speed_level * (SpeedLevel.MOTOR_SPEED_LIMIT.value + (180 - direction) / 2)
            self.__port_0_speed = -speed_level * (SpeedLevel.MOTOR_SPEED_LIMIT.value - (180 - direction) / 2)

        elif 270 > direction > 180:
            self.__port_1_speed = speed_level * (SpeedLevel.MOTOR_SPEED_LIMIT.value - (direction - 180) / 2)
            self.__port_0_speed = -speed_level * (SpeedLevel.MOTOR_SPEED_LIMIT.value + (direction - 180) / 2)

        elif 360 >= direction > 270:
            self.__port_1_speed = -speed_level * (SpeedLevel.MOTOR_SPEED_LIMIT.value - (360 - direction) / 2)
            self.__port_0_speed = speed_level * (SpeedLevel.MOTOR_SPEED_LIMIT.value + (360 - direction) / 2)

    def winch_control(self, operation, speed_level):
        speed_level = float(speed_level)
        if self.__is_winch_init and self.__is_shield_init:
            if operation == "rollUp" and self.__winch_length > 0:
                self.__port_2_speed = - SpeedLevel.WINCH_SPEED_LIMIT.value * speed_level

            elif operation == "rollDown" and self.__winch_length < 50:
                self.__port_2_speed = SpeedLevel.WINCH_SPEED_LIMIT.value * speed_level

    def shield_control(self, operation, speed_level):
        speed_level = float(speed_level)
        if self.__is_winch_init and self.__is_shield_init:

            if operation == "shieldUp" and self.__shield_lift < 3.7:
                self.__port_3_speed = - SpeedLevel.SHIELD_SPEED_LIMIT.value * speed_level

            elif operation == "shieldDown" and self.__shield_lift > 0:
                self.__port_3_speed = SpeedLevel.SHIELD_SPEED_LIMIT.value * speed_level

