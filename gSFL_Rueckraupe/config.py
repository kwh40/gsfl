MINI_TRACTOR_DATE_MODEL = {
    "thingId": "s3i:847dc67e-9dad-4415-8e58-819b724a1a8f",
    "policyId": "s3i:847dc67e-9dad-4415-8e58-819b724a1a8f",
    "attributes": {
        "class": "ml40::Thing",
        "name": "gSFL Rueckraupe",
        "roles": [{"class": "fml40::MiniTractor"}],
        "features": [
            {
                "class": "ml40::OperatingHours",
                "total": 121.1
            },
            {
                "class": "ml40::LastServiceCheck",
                "timestamp": "2020-03-20"
            },
            {
                "class": "ml40::Weight",
                "weight": 1400
            },
            {
                "class": "ml40::Dimensions",
                "length": 2.2,
                "width": 1.5,
                "height": 1.205
            },
            {
                "class": "ml40::OrientationRPY",
                "roll": 0,
                "pitch": 0,
                "yaw": 0
            },
            {
                "class": "ml40::Location",
                "longitude": "ditto-feature:id1",
                "latitude": "ditto-feature:id2",
                "orientation": 135
            },
            {
                "class": "fml40::AcceptsMoveCommands"
            },
            {
                "class": "fml40::AcceptsWinchCommands"
            },
            {
                "class": "fml40::AcceptsShieldCommands"
            },
            {
                "class": "fml40::AcceptsFellingSupportJobs"
            },
            {
                "class": "fml40::AcceptsForwardingJobs"
            },
            {
                "class": "ml40::JobList",
                "subFeatures": [
                    {
                        "class": "fml40::ForwardingJob",
                        "status": "Complete"
                    },
                    {
                        "class": "fml40::FellingSupportJob",
                        "status": "InProgress"
                    },
                    {
                        "class": "fml40::FellingSupportJob",
                        "status": "Pending"
                    }
                ]
            },
            {
                "class": "ml40::Composite",
                "targets": [
                    {
                        "class": "ml40::Thing",
                        "name": "Winch",
                        "roles": [{"class": "fml40::Winch"}],
                        "features": [
                            {
                                "class": "ml40::ExpansionLength",
                                "maxLength": "ditto-feature:id3",
                                "currentLength": "ditto-feature:id4"
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "name": "Engine left",
                        "roles": [{"class": "ml40::Engine"}],
                        "features": [
                            {
                                "class": "ml40::RotationalSpeed",
                                "rpm": "ditto-feature:id5"
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "name": "Engine right",
                        "roles": [{"class": "ml40::Engine"}],
                        "features": [
                            {
                                "class": "ml40::RotationalSpeed",
                                "rpm": "ditto-feature:id6"
                            }
                        ]
                    },
                    {
                        "class": "ml40::Thing",
                        "name": "Rückeschild",
                        "roles": [{"class": "fml40::StackingShield"}],
                        "features": [
                            {
                               "class": "ml40::Lift",
                               "minLift": "ditto-feature:id7",
                               "currentLift": "ditto-feature:id8",
                               "maxLift": "ditto-feature:id9"
                            }
                        ]
                    }
                ]
            }
        ]
    },
    "features": {
        "id1": {
            "properties": {
               "longitude": 6.083611
            }
        },
        "id2": {
            "properties": {
                "latitude": 50.775555
            }
        },
        "id3": {
            "properties": {
                "maxLength": 50
            }
        },
        "id4": {
            "properties": {
               "currentLength": 0
            }
        },
        "id5": {
            "properties": {
                "rpm": 0
            }
        },
        "id6": {
            "properties": {
                "rpm": 0
            }
        },
        "id7": {
            "properties": {
               "minLift": 0
            }
        },
        "id8": {
            "properties": {
               "currentLift": 3.7
            }
        },
        "id9": {
            "properties": {
               "maxLift": 3.7
            }
        }
    }
}

CLIENT_SECRET = "c530e2c1-97cb-44b0-a98d-43720ccfe199"
