from s3i import IdentityProvider, Directory, BrokerAMQP, GetValueRequest, GetValueReply, TokenType, APP_LOGGER, setup_logger, S3IBrokerAMQPError
import json
import uuid
import os
import base64
import jwt
from jwt import DecodeError, ExpiredSignatureError, InvalidTokenError
from jwt.algorithms import RSAPSSAlgorithm
import asyncio


def callback(ch, method, properties, body):
    """
    This function will be called and executed every time that S3I Broker delivers messages to the consuming application (HMI). \n
    Result contained in received service reply will be parsed and printed.

    Args:
        ch: channel used during consuming, which binds productor and consumer \n
        method: „method“ contains meta information regarding message delivery: It provides delivery flags, redelivery flags, and a routing key that can be used to acknowledge the message. \n
        properties: properties of queue and exchange which are used during consuming \n
        body: content of message
    """
    if isinstance(body, bytes):
        try:
            service_reply = GetValueReply(base_msg=body)
        except S3IBrokerAMQPError as e:
            APP_LOGGER.error(e)
        else:
            APP_LOGGER.info("Service reply: {}".format(service_reply.base_msg.get("value")))


def authenticate(username, password):
    idp = IdentityProvider(
        grant_type='password',
        identity_provider_url="https://idp.s3i.vswf.dev/",
        realm='KWH',
        client_id="s3i:00921e2b-67bd-4ee7-a047-c95ae6ce17de",
        client_secret="95bf8d46-b261-4a2d-aa86-db78c57d760b",
        username=username,
        password=password
    )
    APP_LOGGER.info("Username and password are sent to S3I IdentityProvider.")
    return idp


def get_token(idp):
    APP_LOGGER.info("Token obtained.")
    return idp.get_token(TokenType.ACCESS_TOKEN)


def verify_token(idp, token):
    idp_keys = idp.get_certs()["keys"]
    public_key = ""
    for key in idp_keys:
        if key["alg"] == "RS256" and key["kty"] == "RSA" and key["use"] == "sig":
            public_key = RSAPSSAlgorithm.from_jwk(json.dumps(key))
            break
    try:
        """decode the access token with public key that was searched above"""
        jwt.decode(
            jwt=token,
            key=public_key,
            algorithms=["RS256"],
            audience="rabbitmq"
        )
    except (DecodeError, ExpiredSignatureError, InvalidTokenError):
        return False
    else:
        return True


def connect_to_broker(token, loop):
    APP_LOGGER.info("Connecting to the broker endpoint at {}".format(sender_endpoint))
    br = BrokerAMQP(
        token=token,
        endpoint=sender_endpoint,
        callback=callback,
        loop=loop
    )
    br.connect()
    return br


def prepare_getvalue_request(receiver):
    msg_uuid = "s3i:" + str(uuid.uuid4())
    getReq = GetValueRequest()
    getReq.fillGetValueRequest(
        sender=sender,
        receivers=[receiver],
        message_id=msg_uuid,
        attribute_path="attributes/name",
        reply_to_endpoint=sender_endpoint
    )

    return getReq


if __name__ == "__main__":
    setup_logger("[S3I][Demo2][Sender]")

    """
    Step 1: Sender tries to authenticate himself against S3I IdentityProvider, in order to obtain an access token.
    In this Step, the grant type is selected as password which is a simply way to get a access token. 
    In a subsequent version, the grant type as authorization code flow will be implemented 
    """
    APP_LOGGER.info("gSFL AMQP Test")
    username = input('[S3I]: Please enter the username: \n')
    password = input('[S3I]: Please enter the password: \n')
    s3i_identity_provider = authenticate(username, password)
    access_token = get_token(s3i_identity_provider)

    """
    Step 2: Decode the access token
    """

    if verify_token(s3i_identity_provider, access_token):
        APP_LOGGER.info("Token is valid")
    else:
        APP_LOGGER.error("Invalid token")


    """
    The id and endpoint of sender's HMI are known  
    """
    sender = "s3i:00921e2b-67bd-4ee7-a047-c95ae6ce17de"
    sender_endpoint = "s3ib://s3i:00921e2b-67bd-4ee7-a047-c95ae6ce17de"

    """
    Step 5: Authentication with access token (JWT) at S3I Broker
    """
    loop = asyncio.get_event_loop()
    broker = connect_to_broker(token=access_token, loop=loop)

    """
    Step 6: prepare the parameter data (ShapeFile)
    """
    gsfl_harvester = "s3i:ef39a0ae-1f4a-4393-9508-ad70a4d38a63"
    gsfl_forwarder = "s3i:08bf8c98-ad4e-4bc1-b823-7667e73688ac"
    gsfl_waldarbeiter_karl = "s3i:5a256363-4e77-4436-8c14-20b147b38819"
    gsfl_waldarbeiter_walter = "s3i:0c253262-428e-44be-a11a-b83566bd1f68"
    get_value_request = [ prepare_getvalue_request(gsfl_harvester),
                          prepare_getvalue_request(gsfl_forwarder),
                          prepare_getvalue_request(gsfl_waldarbeiter_karl),
                          prepare_getvalue_request(gsfl_waldarbeiter_walter)]

    def send_get_value_request():
        for i in get_value_request:
            broker.send(
                endpoints=["s3ib://{}".format(i.base_msg["receivers"][0])],
                msg=json.dumps(i.base_msg)
            )

    """
    Step 7: send the message
    """
    broker.add_on_channel_open_callback(
        send_get_value_request,
        True
    )

    """
    Step 8: Enable the loop 
    """
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        APP_LOGGER.info("Disconnect from S3I")
        loop.close()
